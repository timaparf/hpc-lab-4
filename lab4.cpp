#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"

double f(double x)
{ 
	return sin(cos(x*x))*exp(sin(x*x));
}

int main(int argc, char *argv[])
{	
	double xmin = atof(argv[1]);
	double xmax = atof(argv[2]);
	unsigned long int n = atoi(argv[3]);
	int processId;
	int numprocs;
	unsigned long int i;
	double start_time;
	double end_time;
	double total;
	double integral = 0.0;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs); 
	MPI_Comm_rank(MPI_COMM_WORLD, &processId);
	
	if (processId == 0) start_time = MPI_Wtime();
	MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

	if (n != 0) 
	{
		double h = (xmax - xmin) / n;
		double i1 = processId * (n / numprocs);
		double i2 = (processId + 1) * (n / numprocs);
		integral= (f(xmin + i1*h) + f(xmin + i2*h)) / 2;
		for(i = i1+1 ; i < i2 ; i++)
			integral += f(xmin + i*h);
		}
	MPI_Reduce(&integral, &total, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	if (processId == 0) 
	{
		end_time = MPI_Wtime();
		printf("value of the integral = %f\n", total);
		printf("Execution time: %f s\n", end_time - start_time); fflush(stdout);
	}
	
	MPI_Finalize();
	return 0;
}